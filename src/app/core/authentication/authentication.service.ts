import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

import { Credentials, CredentialsService } from './credentials.service';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private apollo: Apollo, private credentialsService: CredentialsService) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<any> {
    // Replace by proper authentication call
    // const data = {
    //   username: context.username,
    //   token: '123456'
    // };
    // this.credentialsService.setCredentials(data, context.remember);
    // return of(data);

    const email = context.username;
    const password = context.password;

    return this.apollo.mutate<any>({
      mutation: gql`
        mutation Auth($email: String!, $password: String!) {
          login(email: $email, password: $password) {
            success
            error
            message
            token
            expires
          }
        }
      `,
      variables: {
        email,
        password
      }
    });
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }
}
